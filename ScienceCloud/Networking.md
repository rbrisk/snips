# Networking

## Instance with multiple net interfaces

If you create an Ubuntu 18.04 instance with multiple network interfaces, `netplan` will [generate a default route for each of them](https://askubuntu.com/questions/1042582/how-to-set-default-route-with-netplan-ubuntu-18-04-server-2-nic?noredirect=1&lq=1), which would cause intermittent connectivity problems when accessing external locations. To resolve the issue, either one default route should be removed or metric changed.

```
# Check the routing table
ip r s
# Delete one of the default routings
ip r d default via 172.23.255.1
```


