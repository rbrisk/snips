h1. User sty files

- Categories := LaTeX

- Tags := sty, style, user

[[More info|http://tex.stackexchange.com/questions/1137]]

Command that returns the location of user directory.

```shell
kpsewhich -var-value=TEXMFHOME
```

Place the file into the directory (usually @~/texmf@ preserving the structure)

```shell
mkdir -p ~/texmf/tex/latex/textgreek
cp -p textgreek.sty ~/texmf/tex/latex/textgreek
```

This step doesn't seem to be necessary anymore: rebuild the package index.

```shell
texhash ~/texmf
# -OR-
mktexlsr
```

