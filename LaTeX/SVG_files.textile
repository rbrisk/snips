h1. SVG files

- Categories := LaTeX

- Tags := dynamic, graphics, include, svg

SVG files may be dynamically converted to pdf/tex. This allows proper scaling of the text included in the image. svg package checks whether svg file changed since last time and re-converts it into pdf and pdf_tex files if necessary.

In order for it to work correctly, --shell-escape flag must be added

```shell
pdflatex -interaction=nonstopmode --shell-escape source.tex
```

If images are not in the same directory, svgpath has to be set. The file name should be without extension.

```tex
\usepackage[inkscape={/Applications/Inkscape.app/Contents/Resources/bin/inkscape -z -C}]{svg}

\begin{document}
	\includesvg[svgpath=images/,width=\textwidth]{filename}
\end{document}
```

