h1. Beamer overlays

- Categories := LaTeX

- Tags := beamer, overlay, animation

More information [[here|http://www.math-linux.com/latex-26/article/how-to-make-a-presentation-with-latex-introduction-to-beamer]].

Beamer provides several commands for overlays.

h2. Pause

```tex
\begin{itemize}
	\item One
	\pause
	\item Two
\end{itemize}
```

h2. Item

```tex
\begin{itemize}
	\item One
	\item<2-> Two
	\item<3-4> Three
	\item<4-> Four
	\item<5-> Five
\end{itemize}
```

In the simplest case when you want a pause between each item, you can use @[<+->]@ command.

```tex
\begin{itemize}[<+->]
	\item One
	\item Two
\end{itemize}
```

h2. Regular text

Overlays for regular text are done with @uncover@ and @only@. The commands appear equivalent. The syntax is the same as for @item@. 

```tex
This will appear \uncover<2->{later} and it will look \only<3->{weird}.
```

h2. Hiding text

You can hide the text with @invisible@.

```tex
Now you see \invisible<2->{me}. \uncover<2->{Now you don't.}
```

h2. Highlighting

Within text

```tex
\alert<1>{This} \alert<2>{will highlight} \alert<3>{one by one}
```

In the list

```tex
\begin{itemize}
	\item <+-| alert@+> One
	\item <+-| alert@+> Two
\end{itemize}
```

Or with arbitrary colour (note the position of braces in @\textcolor@ vs @\color@)

```tex
The following text {\color<2>{green}will become green} and {\color<2->[rgb]{1,0,0}this text} will be red.
The following text \textcolor<2>{green}{will become green} and \textcolor<2->[rgb]{1,0,0}{this text} will be red.
```

