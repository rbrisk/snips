h1. Sum indices

- Categories := LaTeX

- Tags := indexes, indices, limits, sum

Without fractions, sum works just fine without limits.

```tex
\frac{\sum\limits_{i=1}^{n} a_i}{\sum\limits_{j=1}{m} b_j}
```

