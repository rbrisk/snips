h1. vim

- Categories := Util

- Tags := editor, vi, vim


h2. Navigation

* @25|@ : go to specific column 
* @H@ @M@ @L@ : top, middle, bottom respectively
* @{@ @}@ @(@ @)@ : paragraph, sentence 
* @^@ : first non-blank char 


h2. Search

* @:S hello world@ : spaces, tabs or new lines are allowed between words.
* @:S! hellow world@ : any special characters are allowed between words.
* Special characters in regular search
** @\n@ a new line char
** @\_s@ a whitespace (space or tab) or newline char
** @\_^@ beginning of a line
** @\_$@ end of a line
** @\_.@ any char including a newline


h2. Editing

[[Text objects|http://blog.carbonfive.com/2011/10/17/vim-text-objects-the-definitive-guide/]] are quite powerful in vi. Motion commands operating on a text object including or excluding spaces (@a@ and @i@ respectively) operate on the object regardless of the cursor position within the object.

* @daw@ @diw@ : delete a word including or excluding surrounding characters.
* @cas@ @cis@ : change a sentence or inner sentence.
* @dap@ @dip@ : delete a paragraph or inner paragraph.
* @cat@ @cit@ : change a tag block or inner tag block.
* @ci,w@ : change an inner camel or snake-cased word.
* @daa@ @dia@ : delete an argument or inner argument.

The commands above also work with @"@ @'@ @`@ @]@ @)@ @}@ @>@.

* @%@ : jump to the matching brace.
* @V@ : line wide visual mode
* @Ctrl+r@ : redo 
* @vip@ or @vap@ : select paragraph in visual mode 
* @gq@ : reformat the paragraph selected in visual mode 


h2. Surround

[[vim-surround bundle|https://github.com/tpope/vim-surround]]. In most cases, the cursor should be within the text to be operated upon. See the Editing section above for more details on text objects.

* @cs"'@ : replace "Hello world" with 'Hello world'
* @cs"<q>@ : replace "Hello world" with <q>Hello world</q>
* @cst"@ : surround the text with double quotes
* @cst[@ vs @cst]@ : when using parentheses, opening parenthesis inserts additional spaces around.
* @ds"@ : removes double quotes
* @ysiw)@ vs @yss)@ : Surrounds a single word or the whole line with parentheses
* @S]@ : for visual mode


h2. Folding

* @zo@ @zc@ @za@ : open, close, toggle folding at the cursor
* @zO@ @zC@ @zA@ : open, close, toggle folding on all folding levels
* @zm@ @zr@ : close, open one level of folding



h2. Interface and configuration

* @:set nu@ and @:set nu!@ : line numbers 
* @:set wrap@ and @:set nowrap@ : enable/disable line wrapping 
* @:set encoding?@ : check the current option value 


h2. Spellcheck

* @:set spell spelllang=en_uk@ : spellcheck 
* @:setlocal spell spelllang=en_uk@ : spellcheck for the current buffer only 
* @]s@ or @[s@ : move to the next misspelled word 
* @z=@ : show suggestions 
* @zg@ : add correct word to the dictionary


h2. Windows

* @:[N]sp@ or @Ctrl-w s@ : split the window horizontally and make the new window N high
* @:res N@ : Resize the window to N. If @+@ or @-@ is specified, adjust window by N
* @:vs N@ or @Ctrl-w v@ : split the window vertically and make the new window N wide
* @Ctrl-w x@ : exchange the current window with the next one


h2. Special commands

Many special commands start with so-called @<leader>@. By default, this key is mapped to @\@. The timeout after pressing the @<leader>@ is 1000ms by default but it can be changed with @set timeoutlen=2000@. You can display commands including @<leader>@ containing commands with @:set showcmd@.


h2. Syntax nuisances

Many language syntax files use global variables to prevent the default and in some cases increadibly annoying behaviour for tab expansion, tab width, and line wrapping. Notably, python can be cured with @let g:python_recommended_style = 0@. There are three relevant parameters for R.

```vi
let r_indent_align_args = 0
let r_indent_ess_comments = 0
let r_indent_ess_compatible = 0
```

