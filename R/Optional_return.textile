h1. Optional return

- Categories := R

- Tags := function, optional, return

```r
normData <- function(paramA) {
	# Do something with it
	# Return only when called as "res <- normData(a)"
	invisible(someData)
}
```

