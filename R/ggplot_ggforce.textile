h1. ggplot ggforce

- Categories := R

- Tags := ggplot, ggforce, theme, dark, black


h2. Theme updates

@ggforce@ retrieves various theme elements from the @theme@ object. In particular, @facet_zoom@ is using three properties: @zoom@, @zoom.x@, and @zoom.y@. The purpose of the first one is not clear. The other two are used to highlight and trace the zoomed region. When those properties are not found in the current theme, it copies the value of the @background.strip@ property. For the black theme, the fill is, thus, black and colour is none, which makes the highlight and trace regions almost invisible.

Theme can be easily expanded with those elements but the validation should be turned off.

```r
theme_black() %+replace%
	theme(
		zoom = element_rect(fill = "grey30", colour = NA),
		zoom.x = element_rect(fill = "grey30", colour = "grey40"),
		zoom.y = element_rect(fill = "grey30", colour = "grey40"),
		validate = F
	)
```

