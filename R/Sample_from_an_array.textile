h1. Sample from an array

- Categories := R

- Tags := random, randomize, sample

```r
x <- runif(1000, 0, 1)
sample(x, 10, replace=T)
```

