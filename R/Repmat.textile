h1. Repmat

- Categories := R

- Tags := Repmat, matrix

```r
> matrix(c(1,2,3), nrow=2, ncol=3, byrow=T)
     [,1] [,2] [,3]
[1,]    1    2    3
[2,]    1    2    3


> matrix(c(1,2,3), nrow=3, ncol=2)
     [,1] [,2]
[1,]    1    1
[2,]    2    2
[3,]    3    3
```

