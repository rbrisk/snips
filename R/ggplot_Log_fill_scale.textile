h1. ggplot Log fill scale

- Categories := R

- Tags := ggplot, labels, scale


It is possible to transform the values in bins using @trans@ parameter. For instance, you can obtain log scale with

```r
ggplot(aes(Parent, Polyploid, fill = Count)) +
	geom_bin2d(stat = "identity") +
	scale_fill_viridis_c(trans="log")
```

Often this results in very strange label values, e.g. @c(1,7,38,1497)@. Those can be changed by providing a function to the breaks parameter. The function takes the limits as input and returns the breaks. For example, the following code will yield a sequence like @c(1,10,100,1000)@.

```r
ggplot(aes(Parent, Polyploid, fill = Count)) +
	geom_bin2d(stat = "identity") +
	scale_fill_viridis_c(trans="log", breaks=function(x) 10**seq(0, floor(log10(max(x)))))
```

