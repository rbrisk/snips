h1. Consecutive pairs

- Categories := R

- Tags := dplyr, lag, lead, consecutive

@dplyr@ has @lag@ and @lead@ functions that allow performing operations on consecutive values in a vector.

```r
ds %>%
	group_by(Network) %>%
	mutate(Delta = Fitness - lag(Fitness, default = first(Fitness)))
```

```r
ds %>%
	group_by(Network) %>%
	mutate(Delta = Fitness - lag(Fitness, n = 2, default = 0)
```

