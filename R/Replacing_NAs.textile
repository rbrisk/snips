h1. Replacing NAs

- Categories := R

- Tags := NAs, tidyr

@tidyr@ has a convenient function to replace NAs with another value per column. Example from the help vignette.

```r
ds <- data_frame(x = c(1, 2, NA), y = c("a", NA, "b"))
ds %>% replace_na(list(x = 0, y = "unknown"))
```
