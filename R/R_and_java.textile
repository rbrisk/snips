h1. R and java

- Categories := R

- Tags := jre, rJava, libjvm


There exist R packages, notably rJava, that allow execution of java commands. They generally cause no problems on Linux but represent a major backache on MacOS. To update java version that R would dynamically load, you need to run @R CMD javareconf@. This command updates the files that reside in @/Library/Frameworks/R.framework/Versions/Current/Resources/etc@ such as @javaconf@. If you run this command and install @rJava@ from source, you should be able to load that package from the command-line R. However, such solution is not sufficient for R GUI and RStudio.

Apparently, @rJava@ dynamically links to '@rpath/libjvm.dylib' and the value for rpath should be supplied by the application, which never happens. The default value is the location of R's framework rather than Java framework. So, you get 'Library not loaded: @rpath/libjvm.dylib' error. You can check the dynamic libraries @rJava@ loads with @otool -L rJava.so@. I tried various tricks other people used such as setting @JAVA_HOME@ and @R_JAVA_LD_LIBRARY_PATH@ but nothing worked. Short of symlinking libjvm in @/usr/local/lib@, which is less than ideal to put it mildly, there is only one solution at this point. Before loading the @rJava@ package, you need to load @libjvm@ directly.

```R
dyn.load("/Library/Java/JavaVirtualMachines/jdk1.8.0.jdk/Contents/Home/jre/lib/server/libjvm.dylib")
library(rJava)
```

This is annoying but it works until a better solution is found.

R admin manual has some [[terse information|https://cran.r-project.org/doc/manuals/R-admin.html#Java-support]] on the topic but it is not helpful.
