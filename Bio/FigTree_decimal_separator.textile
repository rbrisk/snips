h1. FigTree decimal separator

- Categories := Bio

- Tags := decimal, figtree, java, locale, mark

The decimal separator is define by the language and can be set on the command line by using @user.language@ parameter. Other locale parameters are @user.country@ and @user.variant@.

```shell
java -Duser.language=gb -jar figtree.jar
```

