h1. Genomic positions

- Categories := Bio

- Tags := genome, position, coordinate, range, sequence

Genomic coordinates are stored differently in different file formats. They can be 0- or 1-based. Ranges may be open or closed. Strand information can use signs or numbers. The lack of strand information may be represented by different symbols. Below is a summary of most often used formats. (RClosed indicates whether the interval is closed on the right.)

| Format    | PosField | 0-1 | RClosed | StrandField | Strand | No Strand |
| bed       |      2,3 |   0 |      no |           6 |    + - |         . |
| GFF/GTF   |      4,5 |   1 |     yes |           7 |    + - |         . |
| GRanges   |       NA |   1 |     yes |          NA |    + - |         * |
| sam       |        2 |   1 |  length |    2 (flag) |   0x10 |        NA |
| Biopython |       NA |   0 |      no |          NA |   1 -1 |         0 |
