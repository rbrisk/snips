h1. Process substitution

- Categories := Bash

- Tags := named pipe, read

[[More info|http://mywiki.wooledge.org/BashGuide/InputAndOutput#Process_Substitution]]

```shell
diff -y <(head -n 1 .dictionary) <(tail -n 1 .dictionary)
```

