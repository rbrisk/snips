h1. File backup copy

- Categories := Bash

- Tags := copy, string, substitution

Brace expansion.

```shell
cp -a file.txt{,.orig}
```

