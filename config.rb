Gollum::Page.send :remove_const, :FORMAT_NAMES if defined? Gollum::Page::FORMAT_NAMES
#Gollum::Page::FORMAT_NAMES = { 
#  :textile => { :name => "Textile", :regexp => /textile/ },
#  :markdown => { :name => "Markdown", :regexp => /md|mkdn?|mdown|markdown/ }
#}
module RedCloth end
module RedCloth::Formatters end
module RedCloth::Formatters::HTML
	def hard_breaks; false; end
end
