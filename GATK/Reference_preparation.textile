h1. Reference preparation

- Categories := GATK

- Tags := index, reference

GATK cannot handle compressed reference sequences.

Create dictionary

```shell
java -jar $rlib/picard-tools/picard.jar CreateSequenceDictionary R=ref.fa O=ref.dict
```

Create an index

```shell
samtools faidx ref.fa
```

