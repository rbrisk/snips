h1. Natural Sort

- Categories := Perl

- Tags := sort, natural, lexical

@sort@ command uses lexical order that places @s9@ after @s91@. This is not always convenient. @Sort::Naturally@ module provides @nsort@ method that sorts text part lexically while numeric part is sorted numerically. There are some [[restrictions||http://search.cpan.org/~bingos/Sort-Naturally-1.03/lib/Sort/Naturally.pm]], however.

```perl
my @sortedKeys = $isNatural ? nsort(keys %allSeq) : sort { $a cmp $b } keys %allSeq;
```

