h1. Multiline String

- Categories := Perl

- Tags := here, here-doc, multi-line

```perl
my $s = <<END;
This is a long
multi-line string
END

print $s;
```

