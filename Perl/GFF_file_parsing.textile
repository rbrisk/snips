h1. GFF file parsing

- Categories := Perl

- Tags := BioPerl, DB, GFF, graphics, hierarchy

This differs from GFF file processing as it preserves structure and allows queries. Also, this is suitable for building SeqFeature graphics.

```perl
use Bio::DB::SeqFeature::Store;
use Bio::DB::SeqFeature::Store::GFF3Loader;

my $path = "file.gff";

my $db = Bio::DB::SeqFeature::Store->new( -adaptor => 'memory');
my $dbLoader = Bio::DB::SeqFeature::Store::GFF3Loader->new( -store => $db );
$dbLoader->load($path);

# ID is stored as load_id tag
my ($id) = $f->get_tag_values("load_id");
# Name becomes display_name while tag is absent, i.e. $f->has_tag('Name') is false
my $name = $f->display_name;

my @features = $db->features(
	-seq_id    => $seqId,
	-start     => $beg,
	-end       => $end,
	-type      => 'gene',
	-range_type => 'overlaps',
);

# CDS records are already concatenated
my @genes = sort { $a->start <=> $b->start } 
		$db->features( -seq_id => $seq->display_id, -type => 'gene' );
for my $g (@genes) {
	my @mRnas = $g->get_SeqFeatures('mRNA');
	for my $mRna (@mRnas) {
		my ($cds) = $mRna->get_SeqFeatures('CDS');
		my $displayId = $g->display_name;
		my $desc = sprintf("%s:%d..%d", $mRna->seq_id, $mRna->start, $mRna->stop);
		my $cdsSeq = Bio::Seq->new( -id => $displayId, -desc => $desc, -seq => $cds->seq->seq);
		print $cdsSeq, "\n";
	}
}
```

