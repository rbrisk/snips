h1. Object Type

- Categories := Perl

- Tags := bless, class, object, ref, type

[[More Info|http://stackoverflow.com/questions/2091040/how-to-find-which-type-of-object-i-have-on-perl]]

Using @ref@ (reports @HASH@ for unblessed hashtables).

```perl
print ref($obj), "\n";
```

Using @blessed@

```perl
use Scalar::Util;
print blessed($obj), "\n";

```

