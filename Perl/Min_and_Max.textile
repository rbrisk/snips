h1. Min and Max

- Categories := Perl

- Tags := list, max, min, util

```perl
use List::Util qw/ min max/;

my @a = qw/ 5 4 8 3 9 2 /;
print min(@a), "\n";
print max(@a), "\n";
```

