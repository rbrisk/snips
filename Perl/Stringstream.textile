h1. Stringstream

- Categories := Perl

- Tags := Redirecting, Unit Test, stringstream

```perl
my $s = <<END;
Line 1
Line 2
Red
Line 3
END
my $output;

open(SS, "<", \$s) or die("Something is wrong. $!");
open(OUT, ">", \$output) or die("Can't write to a string. $!");

while (<SS>) {
	chomp;
	print OUT $_, " - ok\n" if /^Line \d$/;
}

print $output;
```

