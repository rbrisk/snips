h1. Redirecting output streams

- Categories := Perl

- Tags := Unit Testing, stderr, stdout

```perl
my $outStr;
{
	local *STDOUT;
	open(STDOUT, ">", \$outStr) or die("Unable to redirect stdout to a string. $!");
	# Do something
	close(STDOUT);
}
```

