h1. CDT GCC provider settings

- Categories := Cpp

- Tags := eclipse, CDT, builtin, c++, gcc, path, system

Provider settings need to be reset in @Eclipse/Properties/C-C++/Build/Settings@. Select *Discovery* tab, choose *CDT GCC built-in Compiler Settings* and click *Reset*. This will pick up all GCC system includes using the specified command and those settings will propagate to all projects that use CDT GCC Built-in provider. It is necessary to reset the provider each time GCC is upgraded to the next version.

