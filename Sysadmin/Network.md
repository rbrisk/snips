# Network

## Adding interface

Check if the interface id in `/etc/network/interfaces` is correct. If it is not, update it. There is
no network service, so you have to run `ifdown` followed by `ifup` to source the `interfaces` file.
Starting with v20, it may be necessary to use netplan.

## Routes

Some information is available in this
[Howto](https://www.cyberciti.biz/faq/howto-linux-configuring-default-route-with-ipcommand/).

You can list routes with `ip route list`. To add a route, you can use `ip route add 192.168.1.0/24
dev ens4`. 
