# Mount with systemd

If you put an entry into fstab with `auto` flag, the system may fail to mount if the device is not
available. This is specifically problematic with volumes attached to cloud instances. The safe
but complicated alternative is to mount using `systemd`.

On ubuntu, mounts are specified in `/etc/systemd/system`. You can list the current units with

```
sudo systemctl list-units --type=mount
```

For automatic mounting, two files have to be created. The first file would be the mounting unit, for
example `apps.mount`.

```
[Unit]
Description=opt directory
DefaultDependencies=no
Conflicts=umount.target
Before=local-fs.target umount.target
After=swap.target

[Mount]
What=/dev/disk/by-uuid/xxxxxxxxxxxxxx
Where=/opt
Type=ext4
Options=defaults

[Install]
WantedBy=multi-user.target
```

Below is a sample unit for `cephfs` mount.

```
[Unit]
Description=cephfs apps mount script
Requires=network-online.target
After=network-online.service

[Mount]
What=<cephfs comma separated ips>:/apps
Where=/apps
Options=name=compute,secret=xxxxxxx,mds_namespace=flashfs
Type=ceph

[Install]
WantedBy=multi-user.target
```

Then the automount file `apps.automount`.

```
[Unit]
Description=cephfs apps automount script
Requires=network-online.target
After=network-online.service

[Automount]
Where=/apps
TimeoutIdleSec=1800

[Install]
WantedBy=multi-user.target
```

Finally, you need to create a symlink to the automount file in `multi-user.target.wants`.

```
sudo ln -s /etc/systemd/system/apps.automount multi-user.target.wants/
```

To mount it without rebooting, you need to reload the daemon and start the service.

```
sudo systemctl daemon-reload
sudo systemctl start sapps.mount
```

[More information](https://www.golinuxcloud.com/mount-filesystem-without-fstab-systemd-rhel-8/)

