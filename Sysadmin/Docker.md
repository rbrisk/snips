# Docker

## Running

Run a command in `alpine` container (downloads the container if not found)

```
docker run alpine ls -l
```

Interactive mode

```
docker run -it alpine /bin/sh
```

Launch container. This command works with names or partial unique container ids.

```
docker container start <container_id>
```

Run a command in a running container.

```
docker container exec <container_id> ls -l
```

Stop container

```
docker container stop <container_id>
```



## Container management

List activate containers or all containers

```
docker container ls
docker container ls -a
```

Remove all containers

```
docker container rm $(docker ps -q -a)
```


## Image management

List images

```
docker image ls
```

List files changed in a container

```
docker container diff <container_id>
```

Save changes. Repository is essentially the image name and tag is the version, e.g. `latest`.

```
docker container commit <container_id> [<repository>[:<tag>]]
```

History of the commands in the image

```
docker image history <imgage_id>
```

Remove all images

```
docker image rm $(docker image ls -q)
```

