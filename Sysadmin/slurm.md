# slurm

## QOS

QOS essentially defines the quota groups.

The list of defined QOS.

```
sacctmgr show qos format="name%-20,Description%-30,priority,maxwall"
```

A user can be assigned to multiple QOS. See the [docs](https://slurm.schedmd.com/qos.html)

```
sacctmgr show assoc format=cluster,user,qos
```

If `cgroups` are defined, the kernel only allocates the requested number of CPUs. The other CPUs are not available even though various functions may return the physical number of CPUs.

A QOS can be updated with the following command.

```
sacctmgr modify user where name=<user> account=<account> partition=<partition> set qos=verylong,long,medium,normal
```


## Accounts

You can see all accounts a user is associated with by running

```
sacctmgr show assoc format=cluster,account,partition,user,qos user=<user>
```

Show default account

```
sacctmgr show User format=DefaultAccount user=<user>
```

Set default account

```
sacctmgr modify user where name=<user> set DefaultAccount=<account>
```

Delete user from account

```
sacctmgr delete user name=<user> partition=generic account=<account>
```


## Interactive sessions

Interactive sessions can be launched using `srun`.

```
srun --time=1:0:0 --mem=1G --nodes=1 --cpus-per-task=2 --pty /bin/bash
```


## Jobs

List of recently failed jobs and nodes they ran on.

```
sacct -u xxxxx -S 2019-11-07 -s F -o jobid,state,NodeList
```


## Nodes

List node info.

```
sinfo -N -l -p generic
```

Bring the drained node back into the pool after the error has been fixed.

```
scontrol update NodeName=cloudworker01 state=resume
scontrol update NodeName=cloudworker[01-30] state=resume
```

Issue commands to groups of hosts in parallel. `-f1` would limit the number of parallel commands to
`1`. The default is `32`.

```
pdsh -f1 -w "skylake[1-16] cloudworker[01-30]" 'df -kh | grep vda1'
```


## Reservations

```
scontrol create reservation starttime=2020-07-07T8:00:00 endtime=2020-07-07T20:00:00 \
	accounts=xxxx flags=maint nodes=ALL
```


## Extend job run time

```
scontrol update jobid=<job_id> TimeLimit=14-0:0:0
```


## Kill jobs

Remove all jobs of a user.

```
scancel -u <user_id>
```

