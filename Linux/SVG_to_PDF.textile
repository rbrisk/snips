h1. SVG to PDF

- Categories := Linux

- Tags := conversion, image, pdf, svg, vector

@librsvg@ provides a tool for converting an SVG file into PDF without rasterisation

```shell
rsvg-convert -f pdf -o a.pdf a.svg
```

