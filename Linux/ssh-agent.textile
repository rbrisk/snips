h1. ssh-agent

- Categories := Linux

- Tags := ssh, agent, key


[[More info|http://docstore.mik.ua/orelly/networking_2ndEd/ssh/ch06_03.htm]]

h2. Initialise

When the agent is initialised, it exports @SSH_AUTH_SOCK@ and @SSH_AGENT_PID@. (That is the reason for calling eval as a program cannot export environment variables to the parent shell.) The other three SSH variables (@SSH_CLIENT@, @SSH_TTY@ and @SSH_CONNECTION@) may also have to match but I did not test.

```shell
eval `ssh-agent -s`
```

h2. Termination

The agent is not terminated when you log out. You can terminate it in @.bash_logout@ but that may not work well with tmux.

```shell
ssh-agent -k
```

h2. Adding keys

```shell
ssh-add ~/.ssh/id_rsa
```

