h1. Sort by column

- Categories := Linux

- Tags := column, numeric, sort

[[More info|http://www.skorks.com/2010/05/sort-files-like-a-master-with-the-linux-sort-command-bash/]]

This will sort first by the second column using numeric sort then it will sort by columns 4-6 using regular string sort. 

```shell
sort -k 2n,2 -k 4,6 -o output.txt input.txt
```

The stop is required for @-k@ flag. If not provided, sort uses all columns through the end of line. The following command will sort by all fields from 2 onward.

```shell
# WRONG
sort -k 2 -k 4,6
```

To sort tab-delimited file, add dollar sign. More info is [[here|http://stackoverflow.com/questions/1037365/unix-sort-with-tab-delimiter]] and [[here|http://www.gnu.org/software/bash/manual/bashref.html#ANSI_002dC-Quoting]].

```shell
sort -t$'\t' -k 3 input.txt
```

