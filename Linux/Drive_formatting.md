# Drive formatting

KDE partition manager often fails to complete the operation for unknown reasons. It returns a failure but does not provide any helpful error message.

It is possible to reformat a drive from the command line. Below is an example of formatting a drive with DOS partition table and two partitions. Some additional details can be found at [How-to Geek](https://www.howtogeek.com/106873/how-to-use-fdisk-to-manage-partitions-on-linux/).

1. Find your device

```
lsblk
# or
sudo fdisk -l
```

2. Make sure the drive is unmounted

```
sudo umount /run/media/xxxxx/pivnyk
```

3. Launch `fdisk` in command line mode

```
sudo fdisk /dev/sdb
```

- Create a new DOS partition table with `o`
- Add new partition with `n`
- Accept the default `p` or change the type to `e` if you want multiple logical partitions underneath
- Partition number is 1 by default
- Leave the default for the first sector
- Set the correct size for the last sector, e.g. `+150G` or `+1T`
- Request the type change with `t`
- Set the type to `W95 FAT32 (LBA)` by using `c`. You can list the codes with `L`
- Add another partition with `n`
- Repeat the process but you can leave the default for the last sector unless you want to leave some
  free space
- Print the current table with `p` and make sure it is correct
- Save the table and exit with `w`

4. Format the partitions. **Important**: do not mount the disk or bad things with happen. Not really but you will probably have to start from scratch.

```
sudo mkfs.vfat -F 32 -n muzyka /dev/sdb1
sudo mkfs.exfat -n data /dev/sdb2
```

5. Check the results with `lsblk`

