h1. pandas io

- Categories := Python

- Tags := pandas, io, feather, hdf5, write, read


In the snippets below, pandas is imported as @import pandas as pd@


h2. Compression

If compatibility with R is necessary, @feather@ and @hdf@ with @fixed@ format are the best. R packages (e.g. @hdf5r@) cannot process the @table@ (pytable) format and dataframes in @fixed@ format cannot be queried, i.e. you have to read the whole data frame. It seems that @feather@ format can actually be queried in R but not in python.

Both @read_csv@ and @read_table@ methods accept @compression@ parameter. Its default value is @'infer'@, so there is no need to specify it as long as the compression type ('gzip', 'bz2', 'zip', 'xz') matches the file extension ('gz', 'bz2', 'zip', 'xz'). It could also be set to @None@ to disable compression.

The @compression@ parameter is not mentioned in @to_csv@ documentation. However, the data is compressed if you provide specific compression. As of v0.22, @'infer'@ is not working.

```python
extMap = {"gz": "gzip", "bz2": "bz2", "zip": "zip", "xz": "xz"}
pathOut = "test.txt.xz"
compType = extMap.get(pathOut.split(".")[-1], None)
ds.to_csv(pathOut, sep = "\t", index = False, compression = compType)
```

Among the other file formats, @feather@ is really fast but provides no compression at all. The others (@hdf@, @parquet@) provide moderate compression but not better than gzip. Even though there are many compression algorithms available for @hdf@, they are all rather weak. It is also not clear whether they are actually used because in case of a missing compression library it uses @zlib@ without any warnings.

Example using @complevel = 6@:

- txt + xz 136 Mb
- parquet + brotli 419 Mb
- hdf + blosc:zstd 436 Mb
- hdf + std 452 Mb
- feather 925 Mb (98 Mb after compression with xz, which is very slow)


h2. Compression testing

More careful testing with a rather small file. You can also see some results in [[pandas documentation|https://pandas.pydata.org/pandas-docs/stable/user_guide/io.html#io-perf]].

```python
# The dataframe has about 663 columns with mostly float data
expDataF = expData.iloc[1:5000]
```

```python
def test_hdf_fixed_write(df):
	df.to_hdf('test_fixed.hdf', 'test', mode='w')
	
def test_hdf_fixed_read():
	pd.read_hdf('test_fixed.hdf', 'test')
	
def test_hdf_fixed_write_compress(df):
	df.to_hdf('test_fixed_compress.hdf', 'test', mode='w', complib='blosc:zstd', complevel=6)

def test_hdf_fixed_read_compress():
	pd.read_hdf('test_fixed_compress.hdf', 'test')

def test_hdf_table_write(df):
	df.to_hdf('test_table.hdf', 'test', mode='w', format='table')

def test_hdf_table_read():
	pd.read_hdf('test_table.hdf', 'test')

def test_hdf_table_write_compress(df):
	df.to_hdf('test_table_compress.hdf', 'test', mode='w',
			complib='blosc:zstd', complevel=6, format='table')

def test_hdf_table_read_compress():
	pd.read_hdf('test_table_compress.hdf', 'test')

def test_feather_write(df):
	df.to_feather('test.feather')
	
def test_feather_read():
	pd.read_feather('test.feather')
	
def test_csv_xz_write(df):
	(df.reset_index()
		.round(4)
		.to_csv('test_csv_xz.txt.xz', sep = "\t", index = False, compression = "xz")
	)

def test_csv_xz_read():
	pd.read_csv('test_csv_xz.txt.xz', sep = "\t")
```

@%timeit@ is a magic function from IPython. The function is also available in Jupyter.

```python
%timeit -r 1 -n 10 test_hdf_fixed_write(expDataF)
%timeit -r 1 -n 10 test_hdf_fixed_write_compress(expDataF)
%timeit -r 1 -n 10 test_hdf_table_write(expDataF)
%timeit -r 1 -n 10 test_hdf_table_write_compress(expDataF)
%timeit -r 1 -n 10 test_feather_write(expDataF.reset_index())
%timeit -r 1 -n 10 test_csv_xz_write(expDataF.reset_index())
```

```python
%timeit -r 1 -n 10 test_hdf_fixed_read()
%timeit -r 1 -n 10 test_hdf_fixed_read_compress()
%timeit -r 1 -n 10 test_hdf_table_read()
%timeit -r 1 -n 10 test_hdf_table_read_compress()
%timeit -r 1 -n 10 test_feather_read()
%timeit -r 1 -n 10 test_csv_xz_read()
```

|>. Method       |>. Write ms  |>. Read ms  |>. Size       |
| hdf fixed      |>.        43 |>.     18.7 |>. 26,501,297 |
| hdf fixed zstd |>.      1150 |>.     66.4 |>. 12,448,308 |
| hdf table      |>.       104 |>.     32.7 |>. 26,636,912 |
| hdf table zstd |>.       692 |>.     55.1 |>. 11,175,108 |
| feather        |>.       197 |>.     27.4 |>. 26,550,632 |
| csv xz         |>.     35200 |>.   1380.0 |>. 12,090,444 |
| csv xz lossy   |>.     14700 |>.    807.0 |>.  3,831,280 |



h2. Appending to HDF

PyTables in HDF format are using fixed-width strings. This does not create problems when the whole DataFrame is written at once. Howver, if you plan to append, you need to specify the minimum length for either all string fields at once or each string field individually. Moreover, this may also be necessary for string indices. This is done via @min_itemsize@ parameter in the @append@ function as described in [[User Guide|http://pandas.pydata.org/pandas-docs/stable/user_guide/io.html#storing-types]].

If you use @'values'@, it applies to all string columns and string fields of multi-index. However, it does not work with a single-field index, for which you have to specify the value using @'index'@ key.

Blanket approach.

```python
fieldLengths = {'values': 25, 'index': 25}
store.append(dsKey, ds, format="t", min_itemsize=fieldLengths)
```

Specific columns.

```python
fieldLengths = {'ColumnA': 10, 'ColumnB': 25, 'ColumnC': 100, 'IndexColumnX': 50}
store.append(dsKey, ds, format='f', min_itemsize=fieldLengths)
```

Single-field index.

```python
fieldLengths = {'index': 10}
store.append(dsKey, ds, format='f', min_itemsize=fieldLengths)
```



h2. Rounding for TSV files

It may be necessary to round the values for @tsv@ to keep the file size in check.

```python
(ds.reset_index()
	.round(4)
	.to_csv("out.txt.xz", sep = "\t", compression = "xz")
)
```

