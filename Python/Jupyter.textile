h1. Jupyter

- Categories := Python

- Tags := Jupyter, configuration


h2. Jupyter configuration

It is possible to set various formatting options for Jupyter cells in @~/.jupyter/nbconfig/notebook.json@ as described in the [[documentation|https://minrk-notebook.readthedocs.io/en/latest/frontend_config.html]]. Full list of options is available from [[CodeMirror|https://codemirror.net/doc/manual.html#option_indentUnit]].

```json
{
  "CodeCell": {
    "cm_config": {
      "indentUnit": 3,
      "tabSize": 3,
      "indentWithTabs": true,
      "lineWrapping": true
    }
  }
}
```


